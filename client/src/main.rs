use windows_sys::{
    core::PCSTR, Win32::Foundation::HANDLE, Win32::Storage::FileSystem::CreateFileA,
    Win32::Storage::FileSystem::WriteFile, Win32::Storage::FileSystem::OPEN_EXISTING,
    Win32::System::SystemServices::GENERIC_READ, Win32::System::SystemServices::GENERIC_WRITE,
};

use std::ffi::c_void;
use std::ptr::null_mut;
use std::env;

fn main() {
    unsafe {
        let mut bytes_written: u32 = 0;
        let args: Vec<String> = env::args().collect();
        let server = &args[1];
        let message = &args[2];

        let pipe = format!("\\\\{server}\\pipe\\ccrusty\0");
        let pipe_name: PCSTR = pipe.as_ptr() as *const u8;
        let clientpipe: HANDLE = CreateFileA(
            pipe_name,
            GENERIC_READ | GENERIC_WRITE,
            0,
            null_mut(),
            OPEN_EXISTING,
            0,
            0,
        );

        WriteFile(
            clientpipe,
            message.as_ptr() as *const c_void,
            message.len() as u32,
            &mut bytes_written,
            null_mut(),
        );
    }
}
