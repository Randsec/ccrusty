# ccrusty
Executing commands via named pipes using rust.

## deploy
Upload server.exe to victim and execute it. Will create the \\.\\pipe\\ccrusty named pipe. Use it to communicate via client.exe and execute commands:

`client.exe <ip> <command>`


## TODO
- [ ] Loop the server.
- [ ] Different execution methods.
- [ ] ...

